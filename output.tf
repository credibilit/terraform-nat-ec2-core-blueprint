output "asg" {
  value = "${module.nat_autohealing_asg.asg}"
}

output "asg_arn" {
  value = "${module.nat_autohealing_asg.asg_arn}"
}

output "launch_configuration" {
  value = "${module.nat_autohealing_asg.launch_configuration}"
}

output "public_ip_id" {
  value = "${aws_eip.nat_public_ip.id}"
}

output "public_ip" {
  value = "${aws_eip.nat_public_ip.public_ip}"
}

output "ec2_role" {
  value = "${module.nat_iam_ec2.role}"
}

output "ec2_role_arn" {
  value = "${module.nat_iam_ec2.role_arn}"
}

output "ec2_instance_profile" {
  value = "${module.nat_iam_ec2.instance_profile}"
}

output "ec2_instance_profile_arn" {
  value = "${module.nat_iam_ec2.instance_profile_arn}"
}

output "security_group" {
  value = "${aws_security_group.nat_traffic.id}"
}
