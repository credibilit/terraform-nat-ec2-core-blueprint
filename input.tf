variable "account" {
  description = "The AWS account number ID"
}

variable "name" {
  type = "string"
  description = "A name for the NAT elements"
}

variable "asg_health_check_grace_period" {
  type = "string"
  description = "The time the Autoscaling Group will wait for instances to be health before start triggering alarms"
  default = 60
}

variable "public_subnets" {
  type = "list"
  description = "A list of lists of public subnets to deploy the nat autoscaling groups, one for each NAT"
}

variable "private_routes" {
  type = "list"
  description = "A list of private routes to associate with the NAT instances"
}

variable "instance_type" {
  type = "string"
  description = "The AWS instance family for the NAT instance"
  default = "t2.nano"
}

variable "key_pair" {
  type = "string"
  description = "The key pair name to associate with the SSH service"
}

variable "additional_security_groups" {
  type = "list"
  description = "A list of additional security groups ids to associate with the NAT instance, this will be merged with the NAT Security Group created by the blueprint"
}

variable "iam_polices" {
  type = "list"
  description = "A list of policies to associate to the EC2 role"
  default = []
}

variable "iam_polices_count" {
  type = "string"
  description = "How many policies are in the nat_iam_polices parameter"
  default = "0"
}

variable "ami" {
  type = "string"
  description = "The AMI to be used on EC2 NAT provisioning"
}

variable "user_data" {
  type = "string"
  description = "The user data to bootstrap the instance operational system"
  default = ""
}

variable "csc_mon" {
  type = "string"
  description = "The CredibiliT tag for monitoring fee"
  default = ""
}

variable "csc_seg" {
  type = "string"
  description = "The CredibiliT tag for security fee"
  default = ""
}

variable "csc_bkp" {
  type = "string"
  description = "The CredibiliT tag for backup fee"
  default = ""
}
