data "aws_subnet" "first_subnet" {
  id = "${var.public_subnets[0]}"
}

resource "aws_security_group" "nat_traffic" {
  name = "${var.name}"
  description = "NAT security group"
  vpc_id = "${data.aws_subnet.first_subnet.vpc_id}"

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
