Generic Core EC2 based NAT TerraForm Blueprint
==============================================

This blueprint do not should be called directly, it just creates the core elements to provisioning NAT EC2 instances but do nothing to associate it with any instance or change things like Source Check Destination. You must use others high levels blueprints which call this one and also use some technique to cover the final steps to consolidate the NAT solution.

# Architecture

The following image represent the architecture which this blueprint construct.

![architecture](https://bytebucket.org/credibilit/terraform-nat-ec2-core-blueprint/raw/3ff6948d9bd963817e8b2ff886a6c2107f8ddec7/docs/architecture.svg?token=b593d3612ad1f85eff9e7031013e53597f349357 "Blueprint resources architecture")

The following elements are created:
- An auto healing ASG/LC to provide the EC2.
- A security group to create a solution for Internet access through the NAT.
- IAM instance profile and EC2 role for the instance.
- An EIP to be used by the NAT to garantee a fix public IP in the case of instance reprovision by the auto healing.

## Dependencies

This template depends from this other blueprints versions to function:

- terraform-ec2-role-blueprint (0.0.1)
- terraform-auto-healing-blueprint.git (2.0.0)

Its also expect some resources to be created as parameters:

- A list of public subnets (to deploy the NAT instance)
- A list of private route tables (to feed the AutoHealingConfig tag)
- A list of IAM policies to optionally associate with the instance IAM role (necessary if the instance itself handle the EIP and route tables changing)

## Alternatives

This is not the only way to provide NAT service to your VPC:

- You can use the **AWS NAT Gateway service** instead of a EC2. The service is auto managed and provide the same benefits from this blueprint in terms of auto healing in case of outages with the plus of supporting 10 Gbps of internet traffic. Note the Nat Gateway is more costly than a EC2 so you must plan the use with care. Also see the [terraform-nat-gateway-blueprint](https://bitbucket.org/credibilit/terraform-nat-gateway-blueprint) repository for a more automated solution to build VPC NAT with it.

## Subnets layout

There are two options here which must be chosen based on cost vs high availability:

- Deploy one NAT instance through all Availability Zones (through all public subnets) used by the VPC. This will be called single NAT in this documentation.
- Deploy one NAT in each Availability Zone (one per public subnet) used by the VPC. This will be called multi NAT is this documentation.

The first one is the most cost efficient since only one EC2 instance is launched, the counter point of this is that the AZ where is was deployed or the instance itself suffer an outage there is a time between the detection of the problem and a reprovision by the autoscaling group. During this time, all private instances which must access external services (AWS services, remote API, etc.) will receive errors.

The second is the exact opposite, obviously launching more than one instance will be more costly, but the resilience will be higher. Only one AZ will be affected each time the instance goes down, and if the entire AZ suffers an outage its fine, you will have the others, high? :D.

In general terms, production and staging environments must have multi NAT to ensure high availability and development and homologation single NAT, but this is not a universal law. You must plan the right architecture based on how much you can spend!!!

## Instance type

Choose the right instance type based on the output network traffic for each AZ for multi NAT solutions and for all traffic for single NAT. We recommend look at the [EC2 instance type table](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instance-types.html) and perhaps on non official site [EC2 instances info](http://www.ec2instances.info/).

Non production environment, generally speaking, will have lesser requirements.

## Provisioning

Due to the ephemeral nature of the auto healing using autoscaling group and the need of changing AWS resources to adapt to it, there is a step not provided by this blueprint which must be planned. The steps must include:

- Turn the [Source/Destination Check](http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_NAT_Instance.html#EIP_Disable_SrcDestCheck) off.
- Associate the Elastic IP created by this module to the instance.
- Create the default route in each private route table (provided via parameter) which must use this NAT pointing to it.

There is some ways we come by, you can end with another (better?) solution:

- Using `user_data` parameter to setup the solution. This is used by the [terraform-nat-ec2-simple-blueprint](https://bitbucket.org/credibilit/terraform-nat-ec2-simple-blueprint), see the blueprint for further instructions.
- Use **Chef**, **Puppet**, **Ansible** or other configuration manager of your choice to run when the instance boot.
- Creates and use an ASG notification and run an **AWS Lambda** function to make the changes required. This is a very good solution because you don't need to put IAM policies on the EC2 Role to access and modify EIP attachments, modify EC2 attributes or handle routes inside route tables.

### NAT AMI

We recommend use the AWS NAT AMI, which already provision the `iptables` commands and `sysctl` parameters to activate the packet masquerade and routing. If no exotic disk layout is required by the solution just put something like on **Terraform** this to get the most recent AMI from Amazon:

```terraform
data "aws_ami" "nat_image" {
  most_recent = true
  filter {
    name = "name"
    values = ["amzn-ami-vpc-nat-hvm-*"]
  }
  owners = ["amazon"]
}
```

But if you need to make changings on the disk layout or provision details inside the EC2 like monitoring and customized administration tools we recommend to create an AMI based on the crude NAT AMI with this using [Packer](https://www.packer.io/) or some other similar solution, the following piece of code for **Packer** JSON can retrieve the most recent NAT AMI and then add a [Provisioner](https://www.packer.io/docs/) element to customize it.

```json
"source_ami_filter": {
  "filters": {
    "virtualization-type": "hvm",
    "name": "amzn-ami-vpc-nat-hvm-*",
    "root-device-type": "ebs"
  },
  "owners": ["137112412989"],
  "most_recent": true
}
```

Since the time of provisioning out of AMI can be very high and NAT are a very simple case we do not recommend using configuration managers for this end.

### NAT Security group and allowing Internet traffic from private instances

Beyond just setup routes inside the subnets route tables, you need to allow via **Security Group** the access through NAT instances. This blueprint creates and externalize an Security Group intented to this. This security group can be accessed via the interpolation `${module.NAME.security_group}` where `NAME` is the name given to the module calling this blueprint

There are some approaches which you must consider to do this, without one of then a given instance cannot access the Internet or even the AWS public endpoints (which does not allow then to interact with services like SQS, SNS, OpsWorks, DynamoDB, etc.).

- **Allow by CIDR**: this resembles the classical way to handle firewall/proxies permissions to send traffic out. You can choose a CIDR and put it on a ingress security group rule source like bellow (this is assuming you have an module called `nat` previously created, a VPC with a CIDR `10.0.0.0/16` and correct routes configurations):

```terraform
resource "aws_security_group_rule" "internet_traffic_from_vpc" {
    type = "ingress"
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["10.0.0.0/16"]

    security_group_id = "${module.nat.security_group}"
}
```

This rule is the most simplest and allow traffic of any item with an IP inside the VPC. The counter point here is there are few control over what can access the Internet. You can, although, set rules based on protocols and ports to ensure a more strict way:

```terraform
resource "aws_security_group_rule" "http_from_vpc" {
    type = "ingress"
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["10.0.0.0/16"]

    security_group_id = "${module.nat.security_group}"
}

resource "aws_security_group_rule" "https_from_vpc" {
    type = "ingress"
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["10.0.0.0/16"]

    security_group_id = "${module.nat.security_group}"
}
```

For this function correctly, you must have an pre defined list of all ports and protocols which your applications and services are in need.

In a similar way you can pass subnets CIDR instead of entire VPC CIDR, so you can allow instances in certain subnets have Internet access and others not. For example, you may want allow access from the application layer but not from the database and persistence layer. If so, just put as many rules with the subnets CIDR address you want.

An alternative approach is use security groups as the source instead of subnets. The first way to do this is create a global security group which the NAT security group will use as a source and attribute it to each instance you want to access the Internet. An example follows bellow:

```terraform
resource "aws_security_group" "internet_traffic" {
  name = "internet-traffic"
  description = "Allow send traffic to Internet"

  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group_rule" "allow_traffic_from_internet_sec_group" {
    type = "ingress"
    from_port = 443
    to_port = 443
    protocol = "tcp"
    source_security_group_id = "${aws_security_group.internet_traffic.id}"

    security_group_id = "${module.nat.security_group}"
}
```

An then place the `internet_traffic` on resources like EC2, Launch Configurations, etc. You can combine this with ports and protocols to make it more restrict too.

The second way to work with security groups as a source is use those already attributed for a given purpose. For example, if you have an application with an sec. group (lets say it is called `app`) allowing the ELB access its ports 80 and 443 nothing stops you to use it as a source to the NAT security group.

```terraform
resource "aws_security_group_rule" "allow_traffic_from_internet_sec_group" {
    type = "ingress"
    from_port = 0
    to_port = 0
    protocol = "-1"
    source_security_group_id = "app"

    security_group_id = "${module.nat.security_group}"
}
```
This is quite elegant and allow you to achieve more granular rules then the rules based on CIDR or unique security group for Internet access. But you need to keep more track of the security groups inside your VPC.

**NOTE**: Take in mind you can combine many methods to create complex rules, its all depend of your security compliance level.

# Use

To create a set of NAT EC2 using autoscaling group with 1/1 with this module you need to insert the following peace of code on your own modules:

```
// Call the module
module "<YOUR_MODULE_NAME>" {
  source = "git::https://bitbucket.org/credibilit/terraform_nat_ec2_blueprint.git?ref=<VERSION>"

  ... <parameters> ...
}
```

Where `<VERSION>` is the desired version of *this* module. The master branch store the list of versions which can be used. The possible parameters are listed in advance on this document.

# Parameters

## Input Parameters

The following parameters are used on this module:

- `account`: The AWS account number ID.
- `name`: A name for the NAT elements.
- `asg_health_check_grace_period`: The time the Autoscaling Group will wait for instances to be health before start triggering alarms (default: 60).
- `public_subnets`: A list of lists of public subnets to deploy the nat autoscaling groups, one for each NAT.
- `private_routes`: A list of private routes to associate with the NAT instances.
- `instance_type`: The AWS instance family for the NAT instance.
- `key_pair`: The key pair name to associate with the SSH service.
- `additional_security_groups`: A list of additional security groups ids to associate with the NAT instance, this will be merged with the NAT Security Group created by the blueprint.
- `iam_polices`: A list of policies to associate to the EC2 role (default: []).
- `iam_polices_count`: How many policies are in the nat_iam_polices parameter (default: "0").
- `ami`: The AMI to be used on EC2 NAT provisioning.
- `user_data`: The user data to bootstrap the instance operational system (default: "").
- `csc_mon`: The CredibiliT tag for monitoring fee (default: "").
- `csc_sec`: The CredibiliT tag for security fee (default: "").
- `csc_bkp`: The CredibiliT tag for backup fee (default: "").

## Output parameters

This are the outputs exposed by this module.

- `asg`: The auto healing ASG.
- `asg_arn`: The auto healing AWS ARN.
- `launch_configuration`: The launch configuration to provide NAT instances for the ASG.
- `public_ip_id`: The EIP AWS id.
- `public_ip`: The NAT public IP.
- `ec2_role`: The EC2 Role for NAT instances.
- `ec2_role_arn`: The EC2 Role AWS ARN.
- `ec2_instance_profile`: The NAT EC2 instance profile.
- `ec2_instance_profile_arn`: The NAT EC2 instance profile ARN.
- `security_group`: The security group created to create Internet access permissions.

# TODOs
