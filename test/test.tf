/* Unit test file for module
 * =========================
 * Declare the folowing variables in environment to execute this code:
 * - AWS_ACCESS_KEY_ID
 * - AWS_SECRET_ACCESS_KEY
 * - AWS_DEFAULT_REGION
 * - AWS_SECURITY_TOKEN (if applicable)
 */

/* AWS account
 * Declare this on TF_VAR_account environment variable with your AWS account id
 * for the test account
 */
variable account {}

// Get the AZs from Amazon
data "aws_availability_zones" "azs" { }

variable "name" {
  default = "nat-ec2-core-acme"
}

// A sample VPC
module "vpc" {
  source = "git::https://bitbucket.org/credibilit/terraform-vpc-blueprint.git?ref=0.0.8"

  account = "${var.account}"
  name = "${var.name}"
  domain_name = "${var.name}.local"
  cidr_block = "10.0.0.0/16"
  azs = "${data.aws_availability_zones.azs.names}"
  az_count = 2
  hosted_zone_comment = "An internal hosted zone for testing"
  public_subnets_cidr_block = [
    "10.0.0.0/24",
    "10.0.1.0/24"
  ]
}

resource "aws_security_group" "ssh" {
  name = "ssh-${var.name}"
  description = "Allow all ssh traffic"
  vpc_id = "${module.vpc.vpc}"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_key_pair" "kp" {
  key_name = "${var.name}"
  public_key = "${file("keys/sample-keys-do-not-use-in-production.pub")}"
}

data "aws_ami" "nat_image" {
  most_recent = true

  filter {
    name = "name"
    values = [ "amzn-ami-vpc-nat-hvm-*" ]
  }

  owners = [ "amazon" ]
}

// Call the module
module "test_nat_ec2" {
  source = "../"

  account = "${var.account}"
  name = "${var.name}"
  ami = "${data.aws_ami.nat_image.id}"
  public_subnets = ["${module.vpc.public_subnets}"]
  private_routes = ["${module.vpc.private_route_tables}"]
  key_pair = "${aws_key_pair.kp.key_name}"
  additional_security_groups = ["${aws_security_group.ssh.id}"]
  user_data = "#!/bin/bash"
}

output "asg" {
  value = "${module.test_nat_ec2.asg}"
}

output "asg_arn" {
  value = "${module.test_nat_ec2.asg_arn}"
}

output "launch_configuration" {
  value = "${module.test_nat_ec2.launch_configuration}"
}

output "public_ip_id" {
  value = "${module.test_nat_ec2.public_ip_id}"
}

output "public_ip" {
  value = "${module.test_nat_ec2.public_ip}"
}

output "ec2_role" {
  value = "${module.test_nat_ec2.ec2_role}"
}

output "ec2_role_arn" {
  value = "${module.test_nat_ec2.ec2_role_arn}"
}

output "ec2_instance_profile" {
  value = "${module.test_nat_ec2.ec2_instance_profile}"
}

output "ec2_instance_profile_arn" {
  value = "${module.test_nat_ec2.ec2_instance_profile_arn}"
}

output "security_group" {
  value = "${module.test_nat_ec2.security_group}"
}
