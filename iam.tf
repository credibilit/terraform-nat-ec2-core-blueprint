module "nat_iam_ec2" {
  source = "git::https://bitbucket.org/credibilit/terraform-ec2-role-blueprint.git?ref=0.0.1"

  account = "${var.account}"
  name = "${var.name}"
  policies_arn = "${var.iam_polices}"
  policies_count = "${var.iam_polices_count}"
}
