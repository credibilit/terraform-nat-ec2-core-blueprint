module "nat_autohealing_asg" {
  source = "git::https://bitbucket.org/credibilit/terraform-auto-healing-blueprint.git?ref=2.0.4"
  account = "${var.account}"
  name = "${var.name}"
  subnets = ["${var.public_subnets}"]
  associate_public_ip_address = true
  instance_type = "${var.instance_type}"
  instance_profile = "${module.nat_iam_ec2.instance_profile}"
  security_groups= ["${compact(concat(list(aws_security_group.nat_traffic.id), var.additional_security_groups))}"]
  key_pair = "${var.key_pair}"
  ami = "${var.ami}"
  user_data = "${var.user_data}"

  csc_mon = "${var.csc_mon}"
  csc_seg = "${var.csc_seg}"
  csc_bkp = "${var.csc_bkp}"

  auto_healing_config = [
    {
      routes = "${join(",", var.private_routes)}"
      eip = "${aws_eip.nat_public_ip.id}"
    }
  ]
}
